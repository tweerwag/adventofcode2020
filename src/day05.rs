pub fn main(part: usize, input: &str) -> String {
    let mut seat_ids = parse_boarding_passes(input);
    format!(
        "{}",
        match part {
            1 => seat_ids.iter().copied().max().unwrap(),
            2 => {
                seat_ids.sort_unstable();
                seat_ids
                    .windows(2)
                    .map(|s| match *s {
                        [x, y] => (x, y),
                        _ => unreachable!(),
                    })
                    .find(|(x, y)| *x + 2 == *y)
                    .map(|(x, _)| x + 1)
                    .unwrap()
            }
            _ => panic!("Unknown part number"),
        }
    )
}

fn parse_boarding_passes(input: &str) -> Vec<usize> {
    input
        .lines()
        .map(|l| {
            let row = l[..7]
                .chars()
                .fold(0, |acc, x| 2 * acc + (if x == 'B' { 1 } else { 0 }));
            let col = l[7..]
                .chars()
                .fold(0, |acc, x| 2 * acc + (if x == 'R' { 1 } else { 0 }));
            8 * row + col
        })
        .collect()
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_examples() {
        let s = "FBFBBFFRLR\nBFFFBBFRRR\nFFFBBBFRRR\nBBFFBBFRLL";
        assert_eq!(parse_boarding_passes(s), vec![357, 567, 119, 820]);
    }

    #[test]
    fn test_answers() {
        let input = read_to_string("inputs/day05.txt").unwrap();

        assert_eq!(main(1, &input), "944");
        assert_eq!(main(2, &input), "554");
    }
}
