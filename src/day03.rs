pub fn main(part: usize, input: &str) -> String {
    let width = input.lines().next().unwrap().chars().count();
    let matrix = input
        .lines()
        .map(|s| s.chars())
        .flatten()
        .map(|c| c == '#')
        .collect::<Vec<_>>();
    assert!(matrix.len() % width == 0);

    format!(
        "{}",
        match part {
            1 => do_problem(width, &matrix, &[(3, 1)]),
            2 => do_problem(width, &matrix, &[(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]),
            _ => panic!("Unknown part number"),
        }
    )
}

fn do_problem(width: usize, matrix: &[bool], slopes: &[(usize, usize)]) -> usize {
    let height = matrix.len() / width;
    let mut res = 1;
    for &(step_x, step_y) in slopes {
        let mut cur_x = 0;
        let mut cur_y = 0;
        let mut num_trees = 0;
        while cur_y < height {
            if matrix[cur_x + width * cur_y] {
                num_trees += 1;
            }
            cur_x = (cur_x + step_x) % width;
            cur_y += step_y;
        }
        res *= num_trees;
    }
    res
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_example() {
        let input = "..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#";

        assert_eq!(main(1, input), "7");
        assert_eq!(main(2, input), "336");
    }

    #[test]
    fn test_answers() {
        let input = read_to_string("inputs/day03.txt").unwrap();

        assert_eq!(main(1, &input), "294");
        assert_eq!(main(2, &input), "5774564250");
    }
}
