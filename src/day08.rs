use std::collections::BTreeSet;

use regex::Regex;

#[derive(Clone, Copy, PartialEq, Debug)]
enum InstructionKind {
    Acc,
    Jmp,
    Nop,
}

#[derive(Clone, Copy, PartialEq, Debug)]
struct Instruction {
    kind: InstructionKind,
    offset: isize,
}

#[derive(Clone, PartialEq, Debug)]
struct Machine {
    prog: Vec<Instruction>,
    pc: isize,
    acc: isize,
}

impl Machine {
    pub fn new(prog: Vec<Instruction>) -> Self {
        Machine {
            prog,
            pc: 0,
            acc: 0,
        }
    }

    pub fn step(&mut self) {
        let Instruction { kind, offset } = self.prog[self.pc as usize];
        match kind {
            InstructionKind::Acc => {
                self.acc += offset;
                self.pc += 1;
            }
            InstructionKind::Jmp => {
                self.pc += offset;
            }
            InstructionKind::Nop => {
                self.pc += 1;
            }
        }
    }

    pub fn get_cur_instr(&self) -> Instruction {
        self.prog[self.pc as usize]
    }

    pub fn set_cur_instr(&mut self, instr: Instruction) {
        self.prog[self.pc as usize] = instr;
    }

    pub fn is_terminated(&self) -> bool {
        self.pc as usize == self.prog.len()
    }
}

pub fn main(part: usize, input: &str) -> String {
    let prog = parse_program(input);

    format!(
        "{}",
        match part {
            1 => part1(&prog),
            2 => part2(&prog),
            _ => panic!("Unknown part number"),
        }
    )
}

fn parse_program(input: &str) -> Vec<Instruction> {
    let re = Regex::new(r"(?P<instr>[^ ]+) (?P<val>[+-][0-9]+)").unwrap();

    input
        .lines()
        .map(|line| {
            let cap = re.captures(line).unwrap();
            let kind = match &cap["instr"] {
                "acc" => InstructionKind::Acc,
                "jmp" => InstructionKind::Jmp,
                "nop" => InstructionKind::Nop,
                _ => panic!("Unknown instruction type \"{}\"", &cap["instr"]),
            };
            let offset = cap["val"].parse().unwrap();
            Instruction { kind, offset }
        })
        .collect()
}

fn part1(prog: &[Instruction]) -> usize {
    let mut indexes_seen = BTreeSet::new();
    let mut machine = Machine::new(prog.to_owned());
    loop {
        if !indexes_seen.insert(machine.pc) {
            // Index already present
            break;
        }
        machine.step();
    }
    machine.acc as usize
}

fn part2(prog: &[Instruction]) -> usize {
    let mut indexes_seen = BTreeSet::new();
    let mut machine = Machine::new(prog.to_owned());
    loop {
        if !indexes_seen.insert(machine.pc) {
            // Index already present
            panic!("Found no solution");
        }

        let cur_instr = machine.get_cur_instr();
        if cur_instr.kind != InstructionKind::Acc {
            // Try changing it
            let mut new_machine = machine.clone();
            new_machine.set_cur_instr(Instruction {
                kind: match cur_instr.kind {
                    InstructionKind::Jmp => InstructionKind::Nop,
                    InstructionKind::Nop => InstructionKind::Jmp,
                    _ => unreachable!(),
                },
                ..cur_instr
            });

            let mut new_idx_seen = indexes_seen.clone();
            new_idx_seen.remove(&machine.pc);

            loop {
                if !new_idx_seen.insert(new_machine.pc) {
                    // Index already present
                    break;
                }
                if new_machine.is_terminated() {
                    return new_machine.acc as usize;
                }

                new_machine.step();
            }
        }

        machine.step();
    }
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_examples() {
        let input = "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6";
        assert_eq!(main(1, input), "5");
        assert_eq!(main(2, input), "8");
    }

    #[test]
    fn test_answers() {
        let input = read_to_string("inputs/day08.txt").unwrap();
        assert_eq!(main(1, &input), "2003");
        assert_eq!(main(2, &input), "1984");
    }
}
