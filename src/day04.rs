use std::collections::BTreeMap;

use regex::Regex;

pub fn main(part: usize, input: &str) -> String {
    let data = parse_passport_data(input);

    format!(
        "{}",
        match part {
            1 => part1(&data),
            2 => part2(&data),
            _ => panic!("Unknown part number"),
        }
    )
}

fn parse_passport_data(input: &str) -> Vec<BTreeMap<String, String>> {
    let mut cur = BTreeMap::new();
    let mut res = Vec::new();
    let re = Regex::new(r"(?P<key>[^ :]+):(?P<val>[^ ]+)").unwrap();

    for line in input.lines() {
        if line.is_empty() {
            res.push(cur);
            cur = BTreeMap::new();
        } else {
            for caps in re.captures_iter(&line) {
                cur.insert(caps["key"].to_owned(), caps["val"].to_owned());
            }
        }
    }

    if !cur.is_empty() {
        res.push(cur);
    }

    res
}

fn part1(data: &[BTreeMap<String, String>]) -> usize {
    const REQUIRED_FIELDS: [&str; 7] = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];

    data.iter()
        .filter(|m| REQUIRED_FIELDS.iter().all(|&k| m.contains_key(k)))
        .count()
}

fn part2(data: &[BTreeMap<String, String>]) -> usize {
    let hgt_re = Regex::new(r"^(?P<val>[0-9]+)(?P<unit>cm|in)$").unwrap();
    let hcl_re = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
    let ecl_re = Regex::new(r"^(amb|blu|brn|gry|grn|hzl|oth)$").unwrap();
    let pid_re = Regex::new(r"^[0-9]{9}$").unwrap();

    let validate_hgt = |s: &str| {
        if let Some(caps) = hgt_re.captures(s) {
            if let Ok(val) = caps["val"].parse::<usize>() {
                match &caps["unit"] {
                    "cm" => (150..=193).contains(&val),
                    "in" => (59..=76).contains(&val),
                    _ => false,
                }
            } else {
                false
            }
        } else {
            false
        }
    };

    data.iter()
        .filter(|m| {
            m.get("byr")
                .and_then(|x| x.parse::<usize>().ok())
                .map(|x| (1920..=2002).contains(&x))
                .unwrap_or(false)
                && m.get("iyr")
                    .and_then(|x| x.parse::<usize>().ok())
                    .map(|x| (2010..=2020).contains(&x))
                    .unwrap_or(false)
                && m.get("eyr")
                    .and_then(|x| x.parse::<usize>().ok())
                    .map(|x| (2020..=2030).contains(&x))
                    .unwrap_or(false)
                && m.get("hgt").map_or(false, |x| validate_hgt(x))
                && m.get("hcl").map_or(false, |x| hcl_re.is_match(x))
                && m.get("ecl").map_or(false, |x| ecl_re.is_match(x))
                && m.get("pid").map_or(false, |x| pid_re.is_match(x))
        })
        .count()
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_examples() {
        let s = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in";
        assert_eq!(main(1, s), "2");

        let s = "eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007";
        assert_eq!(main(2, s), "0");

        let s = "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719";
        assert_eq!(main(2, s), "4");
    }

    #[test]
    fn test_answers() {
        let input = read_to_string("inputs/day04.txt").unwrap();
        assert_eq!(main(1, &input), "192");
        assert_eq!(main(2, &input), "101");
    }
}
