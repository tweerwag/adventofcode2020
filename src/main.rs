use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io;

use clap::Clap;
use lazy_static::lazy_static;

mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day19;
mod day20;
mod day21;
mod day22;
mod day23;
mod day24;
mod day25;
mod util;

lazy_static! {
    static ref PROBLEM_FNS: HashMap<usize, fn(usize, &str) -> String> = (1..)
        .zip(
            [
                day01::main,
                day02::main,
                day03::main,
                day04::main,
                day05::main,
                day06::main,
                day07::main,
                day08::main,
                day09::main,
                day10::main,
                day11::main,
                day12::main,
                day13::main,
                day14::main,
                day15::main,
                day16::main,
                day17::main,
                day18::main,
                day19::main,
                day20::main,
                day21::main,
                day22::main,
                day23::main,
                day24::main,
                day25::main,
            ]
            .iter()
            .copied()
        )
        .collect();
}

#[derive(Clap)]
struct Opts {
    /// Specifies which day to run
    day: usize,

    /// Specifies the part of the day to run
    part: usize,

    /// Specifies a different file to use as input (- for stdin)
    #[clap(short, long)]
    input: Option<String>,
}

fn main() {
    let opts = Opts::parse();

    let input = {
        let mut input_read: Box<dyn io::Read> = if let Some(ref f) = opts.input {
            if f == "-" {
                Box::new(io::stdin())
            } else {
                Box::new(File::open(f).unwrap())
            }
        } else {
            let mut p = env::current_dir().unwrap();
            p.push("inputs");
            p.push(format!("day{:02}.txt", opts.day));
            Box::new(File::open(p).unwrap())
        };
        let mut res = String::new();
        input_read.read_to_string(&mut res).unwrap();
        res
    };

    if let Some(f) = PROBLEM_FNS.get(&opts.day) {
        println!("{}", f(opts.part, &input));
    } else {
        panic!("Unknown problem number");
    }
}
