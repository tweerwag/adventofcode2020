pub fn main(part: usize, input: &str) -> String {
    let mut nums = input
        .lines()
        .map(|l| l.parse().unwrap())
        .collect::<Vec<_>>();
    nums.sort_unstable();
    format!(
        "{}",
        match part {
            1 => part1(&nums),
            2 => part2(&nums),
            _ => panic!("Unknown part number"),
        }
    )
}

fn part1(nums: &[usize]) -> usize {
    for &num in nums {
        let target = 2020 - num;
        if nums.binary_search(&target).is_ok() {
            return num * target;
        }
    }
    panic!("Oops, no number found");
}

fn part2(nums: &[usize]) -> usize {
    for &num1 in nums {
        for &num2 in nums {
            if num1 + num2 > 2020 {
                continue;
            }
            let target = 2020 - num1 - num2;
            if nums.binary_search(&target).is_ok() {
                return num1 * num2 * target;
            }
        }
    }
    panic!("Oops, no number found");
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_examples() {
        let input = "1721\n979\n366\n299\n675\n1456";
        assert_eq!(main(1, input), "514579");
        assert_eq!(main(2, input), "241861950");
    }

    #[test]
    fn test_answers() {
        let input = read_to_string("inputs/day01.txt").unwrap();

        assert_eq!(main(1, &input), "1015476");
        assert_eq!(main(2, &input), "200878544");
    }
}
