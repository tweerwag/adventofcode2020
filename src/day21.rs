use std::collections::{HashMap, HashSet};

use regex::Regex;

type Input = Vec<(HashSet<String>, HashSet<String>)>;

pub fn main(part: usize, input: &str) -> String {
    let parsed = parse_input(input);
    match part {
        1 => format!("{}", part1(&parsed)),
        2 => part2(&parsed),
        _ => panic!("Unknown part number"),
    }
}

fn parse_input(input: &str) -> Input {
    let re = Regex::new(r"(?P<ingredients>.*) \(contains (?P<allergens>.*)\)").unwrap();
    input
        .lines()
        .map(|line| {
            let caps = re.captures(line).unwrap();
            (
                caps["ingredients"].split(' ').map(str::to_owned).collect(),
                caps["allergens"].split(", ").map(str::to_owned).collect(),
            )
        })
        .collect()
}

fn find_ingredient_allergen(input: &Input) -> Vec<(&str, &str)> {
    let all_allergens = input
        .iter()
        .flat_map(|(_, algs)| algs)
        .collect::<HashSet<_>>();
    let all_ingredients = input
        .iter()
        .flat_map(|(ings, _)| ings)
        .collect::<HashSet<_>>();

    let mut possible_ingredients = HashMap::new();
    for &alg in &all_allergens {
        let mut ings = all_ingredients.clone();
        for (ings2, _) in input.iter().filter(|(_, algs)| algs.contains(alg)) {
            ings.retain(|&ing| ings2.contains(ing));
        }
        possible_ingredients.insert(alg, ings);
    }

    let mut ingredient_allergen = vec![];
    while !possible_ingredients.is_empty() {
        // Look for an allergen with a single possible ingredient
        let (&alg, ings) = possible_ingredients
            .iter()
            .find(|(_, ings)| ings.len() == 1)
            .expect("Input does not have a solution");
        let &ing = ings.iter().next().unwrap();
        ingredient_allergen.push((&**ing, &**alg));

        // Remove this allergen and ingredient
        possible_ingredients.remove(alg);
        for (_, ings) in &mut possible_ingredients {
            ings.remove(ing);
        }
    }

    ingredient_allergen
}

fn part1(input: &Input) -> usize {
    let ings = find_ingredient_allergen(input)
        .into_iter()
        .map(|(ing, _)| ing)
        .collect::<HashSet<_>>();
    input
        .iter()
        .flat_map(|(ings, _)| ings)
        .filter(|&ing| !ings.contains(&**ing))
        .count()
}

fn part2(input: &Input) -> String {
    let mut ings = find_ingredient_allergen(input);
    ings.sort_unstable_by_key(|&(_, alg)| alg);
    ings.into_iter()
        .map(|(ing, _)| ing)
        .collect::<Vec<_>>()
        .join(",")
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_examples() {
        let input = "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)";
        assert_eq!(main(1, input), "5");
        assert_eq!(main(2, input), "mxmxvkd,sqjhc,fvjkl");
    }

    #[test]
    fn test_answers() {
        let input = read_to_string("inputs/day21.txt").unwrap();
        assert_eq!(main(1, &input), "1829");
        assert_eq!(
            main(2, &input),
            "mxkh,gkcqxs,bvh,sp,rgc,krjn,bpbdlmg,tdbcfb"
        );
    }
}
