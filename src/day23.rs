use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

struct Item {
    label: usize,
    next: Option<Rc<RefCell<Item>>>,
}

type Input = Vec<usize>;

const NUM_REMOVED: usize = 3;

pub fn main(part: usize, input: &str) -> String {
    let parsed = parse_input(input);
    match part {
        1 => part1(&parsed),
        2 => part2(&parsed),
        _ => panic!("Unknown part number"),
    }
}

fn parse_input(input: &str) -> Input {
    input
        .lines()
        .next()
        .unwrap()
        .chars()
        .map(|c| c.to_digit(10).unwrap() as usize)
        .collect()
}

fn to_linked_list(input: &Input) -> Vec<Option<Rc<RefCell<Item>>>> {
    let mut inp_it = input.iter().copied();
    let mut m = HashMap::new();

    let first = Rc::new(RefCell::new(Item {
        label: inp_it.next().unwrap(),
        next: None,
    }));
    m.insert(first.borrow().label, first.clone());

    let mut prev = first.clone();
    while let Some(label) = inp_it.next() {
        let item = Rc::new(RefCell::new(Item { label, next: None }));
        prev.borrow_mut().next = Some(item.clone());
        m.insert(label, item.clone());
        prev = item;
    }

    prev.borrow_mut().next = Some(first.clone());

    let label_max = m.keys().copied().max().unwrap();
    let mut res = vec![None; label_max + 1];
    for (label, item) in m {
        res[label] = Some(item);
    }
    res
}

fn do_cups(
    input: &mut Vec<Option<Rc<RefCell<Item>>>>,
    start: Rc<RefCell<Item>>,
    num_rounds: usize,
) {
    let mut cur_item = start;
    let label_max = input.len() - 1;

    for _ in 0..num_rounds {
        let removed_first = cur_item.borrow_mut().next.take().unwrap();
        let (removed_labels, removed_last) = {
            let mut v = vec![removed_first.borrow().label];
            let mut it = removed_first.clone();
            for _ in 1..NUM_REMOVED {
                let next = it.borrow().next.clone().unwrap();
                it = next;
                v.push(it.borrow().label);
            }
            (v, it)
        };
        let after_removed = removed_last.borrow_mut().next.take().unwrap();

        cur_item.borrow_mut().next = Some(after_removed);

        let dest = {
            let mut dest_label = cur_item.borrow().label;
            loop {
                dest_label -= 1;
                if dest_label == 0 {
                    dest_label = label_max;
                }
                if !removed_labels.contains(&dest_label) {
                    break;
                }
            }
            input[dest_label].as_ref().unwrap()
        };
        let after_dest = dest.borrow_mut().next.replace(removed_first);
        removed_last.borrow_mut().next = after_dest;

        let next = cur_item.borrow().next.clone().unwrap();
        cur_item = next;
    }
}

fn part1(input: &Input) -> String {
    let mut cups = to_linked_list(input);
    let start = cups[input[0]].as_ref().unwrap().clone();
    do_cups(&mut cups, start, 100);

    let mut it = cups[1].as_ref().unwrap().borrow().next.clone().unwrap();
    let mut res = String::new();
    while it.borrow().label != 1 {
        res.push(std::char::from_digit(it.borrow().label as u32, 10).unwrap());
        let next = it.borrow().next.clone().unwrap();
        it = next;
    }
    res
}

fn part2(input: &Input) -> String {
    // First extend the input
    let start = input.len() + 1;
    let input = input
        .iter()
        .copied()
        .chain(start..=1000000)
        .collect::<Vec<_>>();

    let mut cups = to_linked_list(&input);
    let start = cups[input[0]].as_ref().unwrap().clone();

    do_cups(&mut cups, start, 10000000);

    let cup1 = cups[1].as_ref().unwrap().borrow().next.clone().unwrap();
    let cup2 = cup1.borrow().next.clone().unwrap();
    format!("{}", cup1.borrow().label * cup2.borrow().label)
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    #[cfg_attr(debug_assertions, ignore = "runs very slow on debug")]
    fn test_examples() {
        assert_eq!(main(1, "389125467"), "67384529");
        assert_eq!(main(2, "389125467"), "149245887792");
    }

    #[test]
    #[cfg_attr(debug_assertions, ignore = "runs very slow on debug")]
    fn test_answers() {
        let input = read_to_string("inputs/day23.txt").unwrap();
        assert_eq!(main(1, &input), "69425837");
        assert_eq!(main(2, &input), "218882971435");
    }
}
