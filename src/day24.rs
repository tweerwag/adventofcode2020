use std::collections::HashSet;

#[derive(Debug, Copy, Clone, PartialEq)]
enum Direction {
    East,
    SouthEast,
    SouthWest,
    West,
    NorthWest,
    NorthEast,
}

impl Direction {
    fn coord_diff(self) -> (isize, isize, isize) {
        match self {
            Direction::East => (1, -1, 0),
            Direction::SouthEast => (1, 0, -1),
            Direction::SouthWest => (0, 1, -1),
            Direction::West => (-1, 1, 0),
            Direction::NorthWest => (-1, 0, 1),
            Direction::NorthEast => (0, -1, 1),
        }
    }
}

fn neighbours((x, y, z): (isize, isize, isize)) -> impl Iterator<Item = (isize, isize, isize)> {
    use Direction::*;
    const DIRS: &[Direction] = &[East, SouthEast, SouthWest, West, NorthWest, NorthEast];
    DIRS.iter().map(move |&d| {
        let (x2, y2, z2) = d.coord_diff();
        (x + x2, y + y2, z + z2)
    })
}

type Input = Vec<Vec<Direction>>;

pub fn main(part: usize, input: &str) -> String {
    let parsed = parse_input(input);
    match part {
        1 => do_problem(&parsed, 0),
        2 => do_problem(&parsed, 100),
        _ => panic!("Unknown part number"),
    }
}

fn parse_input(input: &str) -> Input {
    input
        .lines()
        .map(|line| {
            let mut res = vec![];
            let mut it = line.chars();
            while let Some(c) = it.next() {
                match c {
                    'n' => {
                        res.push(match it.next().unwrap() {
                            'e' => Direction::NorthEast,
                            'w' => Direction::NorthWest,
                            _ => panic!(),
                        });
                    }
                    's' => {
                        res.push(match it.next().unwrap() {
                            'e' => Direction::SouthEast,
                            'w' => Direction::SouthWest,
                            _ => panic!(),
                        });
                    }
                    'e' => res.push(Direction::East),
                    'w' => res.push(Direction::West),
                    _ => panic!(),
                }
            }
            res
        })
        .collect()
}

fn do_problem(input: &Input, art_rounds: usize) -> String {
    let mut tiles = HashSet::new();
    for line in input {
        let mut cur = (0, 0, 0);
        for dir in line {
            let dir_coord = dir.coord_diff();
            cur.0 += dir_coord.0;
            cur.1 += dir_coord.1;
            cur.2 += dir_coord.2;
        }
        if tiles.contains(&cur) {
            tiles.remove(&cur);
        } else {
            tiles.insert(cur);
        }
    }

    for _ in 0..art_rounds {
        let mut new_tiles = HashSet::new();
        let mut already_checked = HashSet::new();

        for &black_tile in &tiles {
            // Check if black tile remains black
            let n = neighbours(black_tile).filter(|p| tiles.contains(p)).count();
            if n == 1 || n == 2 {
                new_tiles.insert(black_tile);
            }

            // Check if neighbours are flipped to black
            for neighbour in neighbours(black_tile).filter(|p| !tiles.contains(p)) {
                if already_checked.contains(&neighbour) {
                    continue;
                }
                if neighbours(neighbour).filter(|p| tiles.contains(p)).count() == 2 {
                    new_tiles.insert(neighbour);
                }
                already_checked.insert(neighbour);
            }
        }

        tiles = new_tiles;
    }

    format!("{}", tiles.len())
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_examples() {
        let input = "sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew";
        assert_eq!(main(1, input), "10");
        assert_eq!(main(2, input), "2208");
    }

    #[test]
    fn test_answers() {
        let input = read_to_string("inputs/day24.txt").unwrap();
        assert_eq!(main(1, &input), "277");
        assert_eq!(main(2, &input), "3531");
    }
}
