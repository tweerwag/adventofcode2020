use std::collections::VecDeque;
use std::iter::once;

const DEFAULT_WINDOW: usize = 25;

pub fn main(part: usize, input: &str) -> String {
    let nums: Vec<usize> = input.lines().map(str::parse).map(Result::unwrap).collect();

    format!(
        "{}",
        match part {
            1 => part1(&nums, DEFAULT_WINDOW),
            2 => part2(&nums, DEFAULT_WINDOW),
            _ => panic!("Unknown part number"),
        }
    )
}

fn part1(nums: &[usize], window: usize) -> usize {
    let mut sums: VecDeque<(usize, Vec<usize>)> = VecDeque::with_capacity(window);

    for (i, &num) in nums.iter().enumerate() {
        if i >= window {
            if !sums
                .iter()
                .any(|(_, prev_sums)| prev_sums.iter().any(|&x| x == num))
            {
                // Couldn't find number as sum of previous numbers in window
                return num;
            }
            sums.pop_front();
        }

        for &mut (prev_num, ref mut prev_sums) in &mut sums {
            prev_sums.push(prev_num + num);
        }
        sums.push_back((num, Vec::with_capacity(window - 1)));
    }

    panic!("Found no solution");
}

fn part2(nums: &[usize], window: usize) -> usize {
    let target_num = part1(nums, window);
    let accumulate = once(0)
        .chain(nums.iter().scan(0, |x, &y| {
            *x += y;
            Some(*x)
        }))
        .collect::<Vec<_>>();

    for (i, &lower) in accumulate.iter().enumerate() {
        for (j, &upper) in accumulate.iter().enumerate().skip(i + 1) {
            if upper - lower == target_num {
                println!(
                    "Found solution: {} - {} = {} between idx {} and {}",
                    upper, lower, target_num, i, j
                );
                let min = nums[i..j].iter().copied().min().unwrap();
                let max = nums[i..j].iter().copied().max().unwrap();
                println!("Min: {}, max: {}", min, max);
                return min + max;
            }
        }
    }

    panic!("Found no solution");
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_examples() {
        let input = [
            35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309,
            576,
        ];
        assert_eq!(part1(&input, 5), 127);
        assert_eq!(part2(&input, 5), 62);
    }

    #[test]
    fn test_answers() {
        let input = read_to_string("inputs/day09.txt").unwrap();
        assert_eq!(main(1, &input), "18272118");
        assert_eq!(main(2, &input), "2186361");
    }
}
