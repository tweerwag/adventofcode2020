use std::collections::{BTreeMap, BTreeSet};

use regex::Regex;

const OUR_BAG: &str = "shiny gold";

pub fn main(part: usize, input: &str) -> String {
    let data = parse_constraints(input);

    format!(
        "{}",
        match part {
            1 => part1(&data),
            2 => part2(&data),
            _ => panic!("Unknown part number"),
        }
    )
}

fn parse_constraints(input: &str) -> BTreeMap<&str, Vec<(usize, &str)>> {
    let mut res = BTreeMap::new();
    let re = Regex::new(r"(?:(?P<num>[0-9]+) )?(?P<color>[^ ]+ [^ ]+) bags?").unwrap();

    for line in input.lines() {
        let mut iter = re.captures_iter(line);
        let key = iter.next().unwrap().name("color").unwrap().as_str();
        let bags = iter
            .filter_map(|c| {
                Some((
                    c.name("num")?.as_str().parse().unwrap(),
                    c.name("color").unwrap().as_str(),
                ))
            })
            .collect();
        res.insert(key, bags);
    }

    res
}

fn part1(data: &BTreeMap<&str, Vec<(usize, &str)>>) -> usize {
    let mut res = 0;

    for &starting_bag in data.keys() {
        let mut stack = vec![starting_bag];
        let mut coll = BTreeSet::new();
        while let Some(bag) = stack.pop() {
            for &(_, new_bag) in &data[bag] {
                if coll.insert(new_bag) {
                    stack.push(new_bag);
                }
            }
        }

        if coll.contains(OUR_BAG) {
            res += 1;
        }
    }

    res
}

fn part2(data: &BTreeMap<&str, Vec<(usize, &str)>>) -> usize {
    let mut num_bags = BTreeMap::new();
    let mut iter = 0;
    loop {
        let mut complete = true;
        println!("=== Iteration {} ===", iter);

        for (&bag, others) in data.iter() {
            if !num_bags.contains_key(bag) {
                if let Some(new_num) = others
                    .iter()
                    .map(|&(other_num, other_bag)| {
                        num_bags.get(other_bag).map(|&x| (x + 1) * other_num)
                    })
                    .collect::<Option<Vec<_>>>()
                    .map(|x| x.into_iter().sum::<usize>())
                {
                    println!("Inserting {} ==> {}", bag, new_num);
                    num_bags.insert(bag, new_num);
                } else {
                    println!("Bag {} not yet complete", bag);
                    complete = false;
                }
            }
        }

        if complete {
            break;
        }
        iter += 1;
    }

    num_bags[OUR_BAG]
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_examples() {
        let input = "light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.";
        assert_eq!(main(1, input), "4");
        assert_eq!(main(2, input), "32");

        let input = "shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.";
        assert_eq!(main(2, input), "126");
    }

    #[test]
    fn test_answers() {
        let input = read_to_string("inputs/day07.txt").unwrap();
        assert_eq!(main(1, &input), "254");
        assert_eq!(main(2, &input), "6006");
    }
}
