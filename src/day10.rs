use std::iter::once;

pub fn main(part: usize, input: &str) -> String {
    let nums: Vec<usize> = input.lines().map(str::parse).map(Result::unwrap).collect();

    format!(
        "{}",
        match part {
            1 => part1(&nums),
            2 => part2(&nums),
            _ => panic!("Unknown part number"),
        }
    )
}

fn part1(nums: &[usize]) -> usize {
    let device_joltage = nums.iter().copied().max().unwrap() + 3;
    let mut nums: Vec<usize> = once(0)
        .chain(nums.iter().copied())
        .chain(once(device_joltage))
        .collect();
    nums.sort_unstable();

    let mut diff1s = 0;
    let mut diff3s = 0;

    for sl in nums.windows(2) {
        if let [x, y] = *sl {
            match y - x {
                1 => diff1s += 1,
                2 => {}
                3 => diff3s += 1,
                _ => panic!("Cannot use all adapters"),
            }
        } else {
            unreachable!();
        }
    }

    diff1s * diff3s
}

fn part2(nums: &[usize]) -> usize {
    let device_joltage = nums.iter().copied().max().unwrap() + 3;
    let mut nums: Vec<usize> = once(0)
        .chain(nums.iter().copied())
        .chain(once(device_joltage))
        .collect();
    nums.sort_unstable();

    let mut combs = vec![0; nums.len()];
    *combs.last_mut().unwrap() = 1;
    for (i, &num) in nums.iter().enumerate().rev().skip(1) {
        for (j, _) in nums
            .iter()
            .enumerate()
            .skip(i + 1)
            .take_while(|(_, &other_num)| other_num - num <= 3)
        {
            combs[i] += combs[j];
        }
    }

    combs[0]
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_examples() {
        let input = [16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4];
        assert_eq!(part1(&input), 35);
        assert_eq!(part2(&input), 8);

        let input = [
            28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35,
            8, 17, 7, 9, 4, 2, 34, 10, 3,
        ];
        assert_eq!(part1(&input), 220);
        assert_eq!(part2(&input), 19208);
    }

    #[test]
    fn test_answers() {
        let input = read_to_string("inputs/day10.txt").unwrap();
        assert_eq!(main(1, &input), "1656");
        assert_eq!(main(2, &input), "56693912375296");
    }
}
