use std::collections::HashSet;

use lazy_static::lazy_static;

pub fn main(part: usize, input: &str) -> String {
    let (width, board) = parse_input(input);
    format!(
        "{}",
        match part {
            1 => part1(width, &board),
            2 => part2(width, &board),
            _ => panic!("Unknown part number"),
        }
    )
}

fn parse_input(input: &str) -> (usize, Vec<bool>) {
    input
        .lines()
        .map(|line| {
            (
                line.len(),
                line.chars().map(|c| c == '#').collect::<Vec<_>>(),
            )
        })
        .fold((0, vec![]), |(_, mut v1), (len, v2)| {
            v1.extend_from_slice(&v2);
            (len, v1)
        })
}

lazy_static! {
    static ref NEIGHBOURS3: Vec<(i32, i32, i32)> = {
        let mut n = vec![];
        for x in -1..=1 {
            for y in -1..=1 {
                for z in -1..=1 {
                    if !(x == 0 && y == 0 && z == 0) {
                        n.push((x, y, z));
                    }
                }
            }
        }
        n
    };
    static ref NEIGHBOURS4: Vec<(i32, i32, i32, i32)> = {
        let mut n = vec![];
        for x in -1..=1 {
            for y in -1..=1 {
                for z in -1..=1 {
                    for w in -1..=1 {
                        if !(x == 0 && y == 0 && z == 0 && w == 0) {
                            n.push((x, y, z, w));
                        }
                    }
                }
            }
        }
        n
    };
}

fn part1(width: usize, board: &[bool]) -> usize {
    let mut active = HashSet::new();
    for y in 0..(board.len() / width) {
        for x in 0..width {
            if board[y * width + x] {
                active.insert((x as i32, y as i32, 0i32));
            }
        }
    }

    let mut max_x = 1 + width as i32;
    let mut max_y = 1 + (board.len() / width) as i32;
    for max_z in 1..=6 {
        let mut new_active = HashSet::new();
        for x in -max_x..=max_x {
            for y in -max_y..=max_y {
                for z in -max_z..=max_z {
                    let cnt = NEIGHBOURS3
                        .iter()
                        .filter(|&&(x2, y2, z2)| active.contains(&(x + x2, y + y2, z + z2)))
                        .count();
                    if active.contains(&(x, y, z)) {
                        if cnt == 2 || cnt == 3 {
                            new_active.insert((x, y, z));
                        }
                    } else {
                        if cnt == 3 {
                            new_active.insert((x, y, z));
                        }
                    }
                }
            }
        }
        active = new_active;
        max_x += 1;
        max_y += 1;
    }

    active.len()
}

fn part2(width: usize, board: &[bool]) -> usize {
    let mut active = HashSet::new();
    for y in 0..(board.len() / width) {
        for x in 0..width {
            if board[y * width + x] {
                active.insert((x as i32, y as i32, 0i32, 0i32));
            }
        }
    }

    let mut max_x = 1 + width as i32;
    let mut max_y = 1 + (board.len() / width) as i32;
    for max_zw in 1..=6 {
        let mut new_active = HashSet::new();
        for x in -max_x..=max_x {
            for y in -max_y..=max_y {
                for z in -max_zw..=max_zw {
                    for w in -max_zw..=max_zw {
                        let cnt = NEIGHBOURS4
                            .iter()
                            .filter(|&&(x2, y2, z2, w2)| {
                                active.contains(&(x + x2, y + y2, z + z2, w + w2))
                            })
                            .count();
                        if active.contains(&(x, y, z, w)) {
                            if cnt == 2 || cnt == 3 {
                                new_active.insert((x, y, z, w));
                            }
                        } else {
                            if cnt == 3 {
                                new_active.insert((x, y, z, w));
                            }
                        }
                    }
                }
            }
        }
        active = new_active;
        max_x += 1;
        max_y += 1;
    }

    active.len()
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    #[cfg_attr(debug_assertions, ignore = "runs very slow on debug")]
    fn test_examples() {
        let input = ".#.\n..#\n###";
        assert_eq!(main(1, input), "112");
        assert_eq!(main(2, input), "848");
    }

    #[test]
    #[cfg_attr(debug_assertions, ignore = "runs very slow on debug")]
    fn test_answers() {
        let input = read_to_string("inputs/day17.txt").unwrap();
        assert_eq!(main(1, &input), "202");
        assert_eq!(main(2, &input), "2028");
    }
}
