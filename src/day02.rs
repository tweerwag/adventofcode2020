use regex::Regex;

pub fn main(part: usize, input: &str) -> String {
    let re = Regex::new(r"(?P<min>\d+)-(?P<max>\d+) (?P<char>[a-z]): (?P<pwd>[a-z]+)").unwrap();
    let parsed_pwds: Vec<(usize, usize, char, String)> = input
        .lines()
        .map(|pwd| {
            let caps = re.captures(pwd).unwrap();
            (
                caps["min"].parse().unwrap(),
                caps["max"].parse().unwrap(),
                caps["char"].chars().next().unwrap(),
                caps["pwd"].to_owned(),
            )
        })
        .collect();
    format!(
        "{}",
        match part {
            1 => part1(&parsed_pwds),
            2 => part2(&parsed_pwds),
            _ => panic!("Unknown part number"),
        }
    )
}

fn part1(pwds: &[(usize, usize, char, String)]) -> usize {
    pwds.iter()
        .filter(|&&(min, max, c, ref s)| {
            let cnt = s.chars().filter(|&c2| c2 == c).count();
            min <= cnt && cnt <= max
        })
        .count()
}

fn part2(pwds: &[(usize, usize, char, String)]) -> usize {
    pwds.iter()
        .filter(|&&(i1, i2, c, ref s)| {
            let at1 = s.chars().nth(i1 - 1).map_or(false, |c1| c1 == c);
            let at2 = s.chars().nth(i2 - 1).map_or(false, |c2| c2 == c);
            at1 ^ at2
        })
        .count()
}

#[cfg(test)]
mod test {
    use super::*;
    use std::fs::read_to_string;

    #[test]
    fn test_answers() {
        let input = read_to_string("inputs/day02.txt").unwrap();

        assert_eq!(main(1, &input), "536");
        assert_eq!(main(2, &input), "558");
    }
}
