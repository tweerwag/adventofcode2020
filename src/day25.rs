type Input = (usize, usize);

pub fn main(part: usize, input: &str) -> String {
    let parsed = parse_input(input);
    match part {
        1 => part1(&parsed),
        _ => panic!("Unknown part number"),
    }
}

fn parse_input(input: &str) -> Input {
    let mut it = input.lines().map(|line| line.parse().unwrap());
    let num1 = it.next().unwrap();
    let num2 = it.next().unwrap();
    (num1, num2)
}

fn part1(&(pub1, pub2): &Input) -> String {
    let mut val = 1;
    let mut loop1 = 0;
    while val != pub1 {
        val = (val * 7) % 20201227;
        loop1 += 1;
    }

    let mut enc = 1;
    for _ in 0..loop1 {
        enc = (enc * pub2) % 20201227;
    }

    format!("{}", enc)
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_examples() {
        let input = "5764801\n17807724";
        assert_eq!(main(1, input), "14897079");
    }

    #[test]
    fn test_answers() {
        let input = read_to_string("inputs/day25.txt").unwrap();
        assert_eq!(main(1, &input), "297257");
    }
}
