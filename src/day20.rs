use std::collections::HashMap;

use regex::Regex;

const WIDTH: usize = 10;
const BOARD_WIDTH: usize = 12;

pub fn main(part: usize, input: &str) -> String {
    let tiles = parse_input(input);
    format!(
        "{}",
        match part {
            1 => part1(&tiles),
            2 => part2(&tiles),
            _ => panic!("Unknown part number"),
        }
    )
}

fn parse_input(input: &str) -> HashMap<usize, Vec<bool>> {
    let re = Regex::new(r"Tile (\d+):").unwrap();
    let mut res = HashMap::new();
    let mut tile_num = 0;
    let mut tile = vec![];

    for line in input.lines() {
        if let Some(caps) = re.captures(line) {
            tile_num = caps[1].parse().unwrap();
        } else if line.is_empty() {
            res.insert(tile_num, tile);
            tile = vec![];
        } else {
            tile.extend(line.chars().map(|c| c == '#'));
        }
    }

    if !tile.is_empty() {
        res.insert(tile_num, tile);
    }

    res
}

/// Returns tiles in row order from top left to bottom right. Second component is rotation: 0 is input order, 1 is rotated clockwise once, etc., 4 is input flipped horizontally, 5 is flipped input rotated clockwise once, etc.
fn puzzle_tiles(tiles: &HashMap<usize, Vec<bool>>) -> Vec<(usize, u8)> {
    type Tile = (u32, u32, u32, u32);

    // First convert tiles to tile edges
    let tiles = tiles
        .iter()
        .map(|(&tile_num, tile)| {
            // Upper and lower edges
            let upper: u32 = tile[..WIDTH]
                .iter()
                .fold(0, |acc, &x| 2 * acc + if x { 1 } else { 0 });
            let lower: u32 = tile[WIDTH * (WIDTH - 1)..]
                .iter()
                .fold(0, |acc, &x| 2 * acc + if x { 1 } else { 0 });

            // Left and right edges
            let left: u32 = tile
                .iter()
                .step_by(WIDTH)
                .fold(0, |acc, &x| 2 * acc + if x { 1 } else { 0 });
            let right: u32 = tile[WIDTH - 1..]
                .iter()
                .step_by(WIDTH)
                .fold(0, |acc, &x| 2 * acc + if x { 1 } else { 0 });

            (tile_num, (upper, right, lower, left))
        })
        .collect::<HashMap<_, _>>();

    fn rec(tiles: &HashMap<usize, Tile>, sol: &mut Vec<(usize, Tile, u8)>) -> bool {
        fn rev_bits(x: u32) -> u32 {
            x.reverse_bits() >> (32 - WIDTH)
        }
        fn rot_right((x, y, z, w): Tile) -> Tile {
            (rev_bits(w), x, rev_bits(y), z)
        }
        fn rot_right_flip_horiz((x, y, z, w): Tile) -> Tile {
            // (rev_bits(x), w, rev_bits(z), y)
            (w, z, y, x)
        }

        if sol.len() == tiles.len() {
            // Found complete solution
            return true;
        } else {
            // Try extending solution
            let remaining = tiles
                .iter()
                .filter(|&(&num, _)| sol.iter().all(|&(num2, _, _)| num != num2))
                .map(|(&num, &tile)| (num, tile))
                .collect::<Vec<_>>();
            for (num, mut tile) in remaining {
                for rot in 0..8 {
                    if rot == 0 {
                        // Leave tile as-is
                    } else if rot == 4 {
                        // Rotate and then flip tile
                        tile = rot_right_flip_horiz(tile);
                    } else {
                        // Rotate tile
                        tile = rot_right(tile);
                    }
                    // Add tile to solution
                    let sol_len = sol.len();
                    sol.push((num, tile, rot));

                    // Check partial solution
                    let mut is_sol = true;
                    if sol_len >= BOARD_WIDTH {
                        // Check top
                        if tile.0 != sol[sol_len - BOARD_WIDTH].1 .2 {
                            is_sol = false;
                        }
                    }
                    if sol_len % BOARD_WIDTH != 0 {
                        // Check left
                        if tile.3 != sol[sol_len - 1].1 .1 {
                            is_sol = false;
                        }
                    }

                    // Recurse
                    if is_sol {
                        // println!("sol.len() = {}, rot = {}", sol.len(), rot);
                        if rec(tiles, sol) {
                            // Found a solution!
                            return true;
                        }
                    }

                    // Remove last tile again
                    sol.pop();
                }
            }
            return false;
        }
    }

    let mut sol = vec![];
    let have_sol = rec(&tiles, &mut sol);
    assert!(have_sol);
    sol.into_iter().map(|(num, _, rot)| (num, rot)).collect()
}

fn part1(tiles: &HashMap<usize, Vec<bool>>) -> usize {
    let sol = puzzle_tiles(tiles);
    [
        0,
        BOARD_WIDTH - 1,
        (BOARD_WIDTH - 1) * BOARD_WIDTH,
        BOARD_WIDTH * BOARD_WIDTH - 1,
    ]
    .iter()
    .map(|&i| sol[i].0)
    .product()
}

fn part2(tiles: &HashMap<usize, Vec<bool>>) -> usize {
    let sol = puzzle_tiles(tiles);

    /// Width of the smaller tiles with border removed.
    const SWIDTH: usize = WIDTH - 2;

    // Compute "rotation matrices"
    let rot_mats: Vec<Vec<usize>> = {
        let mut r = vec![];

        // First unrotated (rot 0)
        r.push({
            let mut m = vec![];
            for i in 1..WIDTH - 1 {
                for j in 1..WIDTH - 1 {
                    m.push(i * WIDTH + j);
                }
            }
            m
        });

        for rot in 1..8 {
            let old_m = &r[rot - 1];
            let mut m = vec![];

            if rot == 4 {
                // Rotate clockwise and then flip horizontally
                for j in 0..SWIDTH {
                    for i in 0..SWIDTH {
                        m.push(old_m[i * SWIDTH + j]);
                    }
                }
            } else {
                // Rotate clockwise
                for j in 0..SWIDTH {
                    for i in (0..SWIDTH).rev() {
                        m.push(old_m[i * SWIDTH + j]);
                    }
                }
            }
            r.push(m);
        }
        r
    };

    // Stitch tiles together
    let mut image: Vec<bool> = {
        let sol_tiles = sol
            .iter()
            .map(|&(tile_num, rot)| {
                let tile = &tiles[&tile_num];
                rot_mats[rot as usize]
                    .iter()
                    .map(|&i| tile[i])
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();

        let mut image = vec![];
        for i in 0..BOARD_WIDTH {
            for i2 in 0..SWIDTH {
                for j in 0..BOARD_WIDTH {
                    image.extend_from_slice(
                        &sol_tiles[i * BOARD_WIDTH + j][i2 * SWIDTH..(i2 + 1) * SWIDTH],
                    );
                }
            }
        }
        image
    };
    const IMAGE_WIDTH: usize = BOARD_WIDTH * SWIDTH;
    assert_eq!(image.len(), IMAGE_WIDTH * IMAGE_WIDTH);

    // Search for pattern
    const PAT_WIDTH: usize = 20;
    let pattern: Vec<bool> = "                  # #    ##    ##    ### #  #  #  #  #  #   "
        .chars()
        .map(|c| c == '#')
        .collect();
    assert_eq!(pattern.len() % PAT_WIDTH, 0);
    let pat_height = pattern.len() / PAT_WIDTH;

    let mut num_matches = 0;
    for rot in 0..8 {
        // println!("==> rot = {}", rot);

        if rot == 0 {
            // Do nothing
        } else if rot == 4 {
            // Rotate clockwise and then flip horizontally
            let old_image = image;
            image = vec![];
            for j in 0..IMAGE_WIDTH {
                for i in 0..IMAGE_WIDTH {
                    image.push(old_image[i * IMAGE_WIDTH + j]);
                }
            }
        } else {
            // Rotate clockwise
            let old_image = image;
            image = vec![];
            for j in 0..IMAGE_WIDTH {
                for i in (0..IMAGE_WIDTH).rev() {
                    image.push(old_image[i * IMAGE_WIDTH + j]);
                }
            }
        }

        for i in 0..(IMAGE_WIDTH - pat_height + 1) {
            for j in 0..(IMAGE_WIDTH - PAT_WIDTH + 1) {
                // Match pattern
                let mut is_match = true;
                'a: for i2 in 0..pat_height {
                    for j2 in 0..PAT_WIDTH {
                        if pattern[i2 * PAT_WIDTH + j2] {
                            // Pixel in pattern is a '#'
                            if !image[(i + i2) * IMAGE_WIDTH + (j + j2)] {
                                // Pixel in image is NOT a '#'
                                is_match = false;
                                break 'a;
                            }
                        }
                    }
                }

                if is_match {
                    // println!("Found match at {}, {}", i, j);
                    num_matches += 1;
                }
            }
        }
    }

    image.iter().filter(|&&b| b).count() - num_matches * pattern.iter().filter(|&&b| b).count()
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_answers() {
        let input = read_to_string("inputs/day20.txt").unwrap();
        assert_eq!(main(1, &input), "19955159604613");
        assert_eq!(main(2, &input), "1639");
    }
}
