pub fn main(part: usize, input: &str) -> String {
    let fmls = parse_input(input);
    format!(
        "{}",
        match part {
            1 => do_problem(&fmls, |op| match op {
                Op::Plus => 0,
                Op::Mul => 0,
            }),
            2 => do_problem(&fmls, |op| match op {
                Op::Plus => 1,
                Op::Mul => 0,
            }),
            _ => panic!("Unknown part number"),
        }
    )
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum Op {
    Plus,
    Mul,
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum Token {
    LParen,
    RParen,
    Op(Op),
    Num(usize),
}

fn parse_input(input: &str) -> Vec<Vec<Token>> {
    input
        .lines()
        .map(|line| {
            let mut res = vec![];
            let mut num = None;
            for c in line.chars() {
                if c.is_digit(10) {
                    match num {
                        None => num = Some(c.to_digit(10).unwrap() as usize),
                        Some(inner_num) => {
                            num = Some(inner_num * 10 + c.to_digit(10).unwrap() as usize)
                        }
                    }
                } else if let Some(inner_num) = num {
                    res.push(Token::Num(inner_num));
                    num = None;
                }

                match c {
                    '(' => res.push(Token::LParen),
                    ')' => res.push(Token::RParen),
                    '+' => res.push(Token::Op(Op::Plus)),
                    '*' => res.push(Token::Op(Op::Mul)),
                    _ => {}
                }
            }
            if let Some(inner_num) = num {
                res.push(Token::Num(inner_num));
            }
            res
        })
        .collect()
}

fn to_rpn<F: Fn(Op) -> u32>(fml: &[Token], prec: F) -> Vec<Token> {
    let mut res = vec![];
    let mut stack = vec![];
    for &tok in fml {
        match tok {
            Token::Num(_) => {
                res.push(tok);
            }
            Token::Op(op) => {
                while let Some(&Token::Op(op2)) = stack.last() {
                    if prec(op2) >= prec(op) {
                        res.push(stack.pop().unwrap());
                    } else {
                        break;
                    }
                }
                stack.push(tok);
            }
            Token::LParen => {
                stack.push(tok);
            }
            Token::RParen => {
                while let Some(tok2) = stack.pop() {
                    if tok2 != Token::LParen {
                        res.push(tok2);
                    } else {
                        break;
                    }
                }
            }
        }
    }

    while let Some(tok) = stack.pop() {
        res.push(tok);
    }
    dbg!(fml, &res);
    res
}

fn do_problem<F: Fn(Op) -> u32>(fmls: &[Vec<Token>], prec: F) -> usize {
    fmls.iter()
        .map(|fml| {
            let rpn = to_rpn(fml, |op| prec(op));

            let mut stack = vec![];
            for &tok in &rpn {
                match tok {
                    Token::Num(num) => stack.push(num),
                    Token::Op(op) => {
                        let x = stack.pop().unwrap();
                        let y = stack.pop().unwrap();
                        stack.push(match op {
                            Op::Plus => x + y,
                            Op::Mul => x * y,
                        });
                    }
                    _ => unreachable!(),
                }
            }
            stack.pop().unwrap()
        })
        .sum::<usize>()
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_examples() {
        assert_eq!(main(1, "1 + 2 * 3 + 4 * 5 + 6"), "71");
        assert_eq!(main(1, "1 + (2 * 3) + (4 * (5 + 6))"), "51");
        assert_eq!(main(1, "2 * 3 + (4 * 5)"), "26");
        assert_eq!(main(1, "5 + (8 * 3 + 9 + 3 * 4 * 3)"), "437");
        assert_eq!(
            main(1, "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"),
            "12240"
        );
        assert_eq!(
            main(1, "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"),
            "13632"
        );

        assert_eq!(main(2, "1 + 2 * 3 + 4 * 5 + 6"), "231");
        assert_eq!(main(2, "1 + (2 * 3) + (4 * (5 + 6))"), "51");
        assert_eq!(main(2, "2 * 3 + (4 * 5)"), "46");
        assert_eq!(main(2, "5 + (8 * 3 + 9 + 3 * 4 * 3)"), "1445");
        assert_eq!(
            main(2, "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"),
            "669060"
        );
        assert_eq!(
            main(2, "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"),
            "23340"
        );
    }

    #[test]
    fn test_answers() {
        let input = read_to_string("inputs/day18.txt").unwrap();
        assert_eq!(main(1, &input), "202553439706");
        assert_eq!(main(2, &input), "88534268715686");
    }
}
