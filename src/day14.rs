use std::collections::BTreeMap;

use regex::Regex;

use crate::util::*;

#[derive(Clone, Copy, Debug)]
enum InitEntry {
    SetMask { or_mask: u64, and_mask: u64 },
    SetMem { addr: u64, value: u64 },
}

pub fn main(part: usize, input: &str) -> String {
    let parsed = parse_input(input);
    format!(
        "{}",
        match part {
            1 => part1(&parsed),
            2 => part2(&parsed),
            _ => panic!("Unknown part number"),
        }
    )
}

fn parse_input(input: &str) -> Vec<InitEntry> {
    let re_mask = Regex::new(r"mask = (?P<mask>[01X]{36})").unwrap();
    let re_mem = Regex::new(r"mem\[(?P<addr>[0-9]+)\] = (?P<val>[0-9]+)").unwrap();

    input
        .lines()
        .map(|l| {
            if let Some(c) = re_mask.captures(l) {
                let mask = &c["mask"];
                InitEntry::SetMask {
                    or_mask: u64::from_str_radix(
                        &mask
                            .chars()
                            .map(|c| if c == '1' { '1' } else { '0' })
                            .collect::<String>(),
                        2,
                    )
                    .unwrap(),
                    and_mask: u64::from_str_radix(
                        &mask
                            .chars()
                            .map(|c| if c == '0' { '0' } else { '1' })
                            .collect::<String>(),
                        2,
                    )
                    .unwrap(),
                }
            } else if let Some(c) = re_mem.captures(l) {
                InitEntry::SetMem {
                    addr: c["addr"].parse().unwrap(),
                    value: c["val"].parse().unwrap(),
                }
            } else {
                panic!("Could not parse \"{}\"", l);
            }
        })
        .collect()
}

fn part1(prog: &[InitEntry]) -> usize {
    let mut cur_and_mask = 0;
    let mut cur_or_mask = 0;
    let mut mem = BTreeMap::new();

    for &instr in prog {
        match instr {
            InitEntry::SetMask { or_mask, and_mask } => {
                cur_or_mask = or_mask;
                cur_and_mask = and_mask;
            }
            InitEntry::SetMem { addr, value } => {
                mem.insert(addr, (value & cur_and_mask) | cur_or_mask);
            }
        }
    }

    mem.values().copied().sum::<u64>() as usize
}

fn floating_addr_iter(addr: u64, x_mask: u64) -> impl Iterator<Item = u64> {
    let x_bits = one_bits_to_idxs(x_mask);
    let base_addr = addr & !x_mask;
    (0..2u64.pow(x_bits.len() as u32)).map(move |mut x| {
        let mut addr = base_addr;
        for i in 0.. {
            if x & 1 == 1 {
                addr |= 1 << x_bits[i];
            }
            x >>= 1;
            if x == 0 {
                break;
            }
        }
        addr
    })
}

fn part2(prog: &[InitEntry]) -> usize {
    let mut cur_or_mask = 0;
    let mut cur_x_mask = 0;
    let mut mem = BTreeMap::new();

    for &instr in prog {
        match instr {
            InitEntry::SetMask { or_mask, and_mask } => {
                cur_or_mask = or_mask;
                cur_x_mask = or_mask ^ and_mask;
            }
            InitEntry::SetMem { addr, value } => {
                for addr in floating_addr_iter(addr | cur_or_mask, cur_x_mask) {
                    mem.insert(addr, value);
                }
            }
        }
    }

    mem.values().copied().sum::<u64>() as usize
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_examples() {
        let input = "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0";
        assert_eq!(main(1, &input), "165");

        let input = "mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1";
        assert_eq!(main(2, &input), "208");
    }

    #[test]
    fn test_answers() {
        let input = read_to_string("inputs/day14.txt").unwrap();
        assert_eq!(main(1, &input), "9296748256641");
        assert_eq!(main(2, &input), "4877695371685");
    }
}
