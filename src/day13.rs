pub fn main(part: usize, input: &str) -> String {
    let parsed = parse_input(input);
    format!(
        "{}",
        match part {
            1 => part1(&parsed),
            2 => part2(&parsed),
            _ => panic!("Unknown part number"),
        }
    )
}

fn parse_input(input: &str) -> (usize, Vec<Option<usize>>) {
    let mut lines = input.lines();
    let depart_time = lines.next().unwrap().parse().unwrap();
    let schedule = lines
        .next()
        .unwrap()
        .split(',')
        .map(|c| c.parse().ok())
        .collect();
    (depart_time, schedule)
}

fn part1(&(depart_time, ref schedule): &(usize, Vec<Option<usize>>)) -> usize {
    let (bus_id, delta_t) = schedule
        .iter()
        .flatten()
        .map(|&b| (b, b - depart_time % b))
        .min_by_key(|&(_, t)| t)
        .unwrap();
    bus_id * delta_t
}

fn extended_euclid(a: i128, b: i128) -> (i128, i128, i128) {
    let mut r = (a, b);
    let mut s = (1, 0);

    while r.1 != 0 {
        let q = r.0 / r.1;
        r = (r.1, r.0 - q * r.1);
        s = (s.1, s.0 - q * s.1);
    }

    let t = if b != 0 { (r.0 - s.0 * a) / b } else { 0 };

    (s.0, t, r.0)
}

fn chinese_remainder(congs: &[(i128, i128)]) -> (i128, i128) {
    assert!(!congs.is_empty());
    let mut congs = congs.iter();
    let (mut sol_a, mut sol_n) = congs.next().unwrap();
    for &(a, n) in congs {
        let (s, t, r) = extended_euclid(sol_n, n);
        assert_eq!(r, 1);
        assert!(s * sol_n + t * n == r);
        sol_a = a * s * sol_n + sol_a * t * n;
        sol_n *= n;
        sol_a = sol_a.rem_euclid(sol_n);
    }
    (sol_a, sol_n)
}

fn part2((_, schedule): &(usize, Vec<Option<usize>>)) -> usize {
    let congruences = schedule
        .iter()
        .copied()
        .enumerate()
        .filter_map(|(i, x)| Some((i as i128, x? as i128)))
        .collect::<Vec<_>>();
    let (a, n) = chinese_remainder(&congruences);
    (n - a) as usize
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_chinese_remainder() {
        assert_eq!(extended_euclid(13, 7), (-1, 2, 1));
        assert_eq!(extended_euclid(7, 13), (2, -1, 1));

        assert_eq!(chinese_remainder(&[(1, 13)]), (1, 13));
        assert_eq!(chinese_remainder(&[(1, 13), (2, 7)]), (79, 91));

        let (a, _) = chinese_remainder(&[(0, 7), (1, 13), (4, 59)]);
        assert_eq!(a.rem_euclid(7), 0);
        assert_eq!(a.rem_euclid(13), 1);
        assert_eq!(a.rem_euclid(59), 4);
    }

    #[test]
    fn test_examples() {
        let input = "939\n7,13,x,x,59,x,31,19";
        assert_eq!(main(1, &input), "295");
        assert_eq!(main(2, &input), "1068781");

        assert_eq!(main(2, "0\n17,x,13,19"), "3417");
        assert_eq!(main(2, "0\n67,7,59,61"), "754018");
        assert_eq!(main(2, "0\n67,x,7,59,61"), "779210");
        assert_eq!(main(2, "0\n67,7,x,59,61"), "1261476");
        assert_eq!(main(2, "0\n1789,37,47,1889"), "1202161486");
    }

    #[test]
    fn test_answers() {
        let input = read_to_string("inputs/day13.txt").unwrap();
        assert_eq!(main(1, &input), "3215");
        assert_eq!(main(2, &input), "1001569619313439");
    }
}
