use std::collections::HashMap;
use std::ops::RangeInclusive;

use regex::Regex;

pub fn main(part: usize, input: &str) -> String {
    let (constraints, my_ticket, other_tickets) = parse_input(input);
    format!(
        "{}",
        match part {
            1 => part1(&constraints, &other_tickets),
            2 => part2(&constraints, &my_ticket, &other_tickets),
            _ => panic!("Unknown part number"),
        }
    )
}

fn parse_input(
    input: &str,
) -> (
    HashMap<String, (RangeInclusive<u32>, RangeInclusive<u32>)>,
    Vec<u32>,
    Vec<Vec<u32>>,
) {
    let mut lines = input.lines();
    let constraint_re = Regex::new(r"(?P<field>[^:]+): (?P<a_min>[0-9]+)-(?P<a_max>[0-9]+) or (?P<b_min>[0-9]+)-(?P<b_max>[0-9]+)").unwrap();

    let mut constraints = HashMap::new();
    while let Some(line) = lines.next() {
        if line.is_empty() {
            break;
        }
        let cap = constraint_re.captures(line).unwrap();
        constraints.insert(
            cap["field"].to_owned(),
            (
                cap["a_min"].parse().unwrap()..=cap["a_max"].parse().unwrap(),
                cap["b_min"].parse().unwrap()..=cap["b_max"].parse().unwrap(),
            ),
        );
    }

    lines.next().unwrap(); // Skip line "your ticket:"
    let my_ticket = lines
        .next()
        .unwrap()
        .split(',')
        .map(|s| s.parse().unwrap())
        .collect();

    lines.next().unwrap(); // Skip empty line
    lines.next().unwrap(); // Skip line "nearby tickets:"
    let other_tickets = lines
        .map(|line| line.split(',').map(|s| s.parse().unwrap()).collect())
        .collect();

    (constraints, my_ticket, other_tickets)
}

fn part1(
    constraints: &HashMap<String, (RangeInclusive<u32>, RangeInclusive<u32>)>,
    other_tickets: &[Vec<u32>],
) -> usize {
    let all_ranges: Vec<RangeInclusive<u32>> = constraints
        .values()
        .flat_map(|(r1, r2)| vec![r1, r2])
        .cloned()
        .collect();
    let is_field_valid = |x| all_ranges.iter().any(|r| r.contains(&x));
    other_tickets
        .iter()
        .flatten()
        .copied()
        .filter(|&x| !is_field_valid(x))
        .sum::<u32>() as usize
}

fn part2(
    constraints: &HashMap<String, (RangeInclusive<u32>, RangeInclusive<u32>)>,
    my_ticket: &[u32],
    other_tickets: &[Vec<u32>],
) -> usize {
    // Discard all undoubtly invalid tickets
    let all_ranges: Vec<RangeInclusive<u32>> = constraints
        .values()
        .flat_map(|(r1, r2)| vec![r1, r2])
        .cloned()
        .collect();
    let is_field_possibly_valid = |x| all_ranges.iter().any(|r| r.contains(&x));
    let valid_tickets: Vec<Vec<u32>> = other_tickets
        .iter()
        .cloned()
        .filter(|t| t.iter().copied().all(is_field_possibly_valid))
        .collect();

    // Map constraint strs to u8
    let keys: Vec<&str> = constraints.keys().map(|x| &**x).collect();

    // Find possible fields for each column
    let possible: Vec<Vec<u8>> = (0..constraints.len())
        .map(|i| {
            constraints
                .iter()
                .filter(|(_, (r1, r2))| {
                    valid_tickets
                        .iter()
                        .all(|v| r1.contains(&v[i]) || r2.contains(&v[i]))
                })
                .map(|(k1, _)| keys.iter().position(|k2| k1 == k2).unwrap() as u8)
                .collect()
        })
        .collect();

    // Find solution for possible
    fn rec(possible: &[Vec<u8>], sol: &mut Vec<u8>) -> bool {
        if sol.len() == possible.len() {
            // Found a complete solution
            return true;
        } else {
            // Try extending
            for &field in &possible[sol.len()] {
                if !sol.contains(&field) {
                    sol.push(field);
                    if rec(possible, sol) {
                        return true;
                    }
                    sol.pop();
                }
            }
            return false;
        }
    }

    let mut sol = vec![];
    let have_sol = rec(&possible, &mut sol);
    assert!(have_sol);
    sol.into_iter()
        .enumerate()
        .filter(|&(_, f)| keys[f as usize].starts_with("departure"))
        .map(|(i, _)| my_ticket[i] as usize)
        .product::<usize>()
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_examples() {
        let input = "class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12";
        assert_eq!(main(1, input), "71");

        let input = "departure class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9";
        assert_eq!(main(2, input), "12");
    }

    #[test]
    #[cfg_attr(debug_assertions, ignore = "runs very slow on debug")]
    fn test_answers() {
        let input = read_to_string("inputs/day16.txt").unwrap();
        assert_eq!(main(1, &input), "23115");
        assert_eq!(main(2, &input), "239727793813");
    }
}
