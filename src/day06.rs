use std::collections::BTreeSet;

use crate::util::IteratorFoldFirst;

pub fn main(part: usize, input: &str) -> String {
    format!(
        "{}",
        parse_declaration_forms(input)
            .into_iter()
            .map(|v| {
                v.into_iter()
                    .my_fold_first(|acc, s| match part {
                        1 => acc.union(&s).copied().collect(),
                        2 => acc.intersection(&s).copied().collect(),
                        _ => panic!("Unknown part number"),
                    })
                    .unwrap()
                    .len()
            })
            .sum::<usize>()
    )
}

fn parse_declaration_forms(input: &str) -> Vec<Vec<BTreeSet<char>>> {
    let mut res = vec![];
    let mut cur = vec![];

    for line in input.lines() {
        if line.is_empty() {
            res.push(cur);
            cur = vec![];
        } else {
            cur.push(line.chars().collect());
        }
    }

    if !cur.is_empty() {
        res.push(cur);
    }
    res
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_examples() {
        let input = "abc

a
b
c

ab
ac

a
a
a
a

b";
        assert_eq!(main(1, input), "11");
        assert_eq!(main(2, input), "6");
    }

    #[test]
    fn test_answers() {
        let input = read_to_string("inputs/day06.txt").unwrap();

        assert_eq!(main(1, &input), "6662");
        assert_eq!(main(2, &input), "3382");
    }
}
