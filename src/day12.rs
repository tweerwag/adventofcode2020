pub fn main(part: usize, input: &str) -> String {
    let instructions = parse_instructions(input);
    format!(
        "{}",
        match part {
            1 => part1(&instructions),
            2 => part2(&instructions),
            _ => panic!("Unknown part number"),
        }
    )
}

fn parse_instructions(input: &str) -> Vec<(char, isize)> {
    input
        .lines()
        .map(|line| (line.chars().next().unwrap(), line[1..].parse().unwrap()))
        .collect()
}

fn part1(instructions: &[(char, isize)]) -> usize {
    let (mut x, mut y) = (0, 0); // pos x is east, pos y is north
    let mut dir = 0; // 0 = east, 90 = north, etc.

    for &(mut c, val) in instructions {
        if c == 'F' {
            c = match dir {
                0 => 'E',
                90 => 'N',
                180 => 'W',
                270 => 'S',
                _ => panic!("Cannot move along direction {}", dir),
            };
        }
        match c {
            'N' => {
                y += val;
            }
            'S' => {
                y -= val;
            }
            'E' => {
                x += val;
            }
            'W' => {
                x -= val;
            }
            'L' => {
                dir = (dir + val).rem_euclid(360);
            }
            'R' => {
                dir = (dir - val).rem_euclid(360);
            }
            _ => {
                panic!("Unknown command '{}'", c)
            }
        }
    }

    x.abs() as usize + y.abs() as usize
}

fn part2(instructions: &[(char, isize)]) -> usize {
    let (mut x, mut y) = (0, 0); // pos x is east, pos y is north
    let (mut w_x, mut w_y) = (10, 1);

    fn rotate(dir: isize, x: isize, y: isize) -> (isize, isize) {
        match dir {
            0 => (x, y),
            90 => (-y, x),
            180 => (-x, -y),
            270 => (y, -x),
            _ => panic!("Cannot rotate by {} degrees", dir),
        }
    }

    for &(c, val) in instructions {
        match c {
            'N' => w_y += val,
            'S' => w_y -= val,
            'E' => w_x += val,
            'W' => w_x -= val,
            'L' => {
                // Destructuring assignment not stable yet :-( (https://github.com/rust-lang/rust/issues/71126)
                let (x0, y0) = rotate(val, w_x, w_y);
                w_x = x0;
                w_y = y0;
            }
            'R' => {
                // Destructuring assignment not stable yet :-( (https://github.com/rust-lang/rust/issues/71126)
                let (x0, y0) = rotate((-val).rem_euclid(360), w_x, w_y);
                w_x = x0;
                w_y = y0;
            }
            'F' => {
                x += val * w_x;
                y += val * w_y;
            }
            _ => panic!("Unknown command '{}'", c),
        }
    }

    x.abs() as usize + y.abs() as usize
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_examples() {
        let input = "F10\nN3\nF7\nR90\nF11";
        assert_eq!(main(1, &input), "25");
        assert_eq!(main(2, &input), "286");
    }

    #[test]
    fn test_answers() {
        let input = read_to_string("inputs/day12.txt").unwrap();
        assert_eq!(main(1, &input), "590");
        assert_eq!(main(2, &input), "42013");
    }
}
