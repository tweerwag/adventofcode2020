use std::collections::HashMap;

pub fn main(part: usize, input: &str) -> String {
    let parsed = parse_input(input);
    format!(
        "{}",
        match part {
            1 => do_problem(&parsed, 2020),
            2 => do_problem(&parsed, 30000000),
            _ => panic!("Unknown part number"),
        }
    )
}

fn parse_input(input: &str) -> Vec<u32> {
    input
        .lines()
        .next()
        .unwrap()
        .split(',')
        .map(|s| s.parse().unwrap())
        .collect()
}

fn do_problem(start_nums: &[u32], num_turns: u32) -> usize {
    assert!(!start_nums.is_empty());
    assert!(num_turns as usize > start_nums.len());

    let mut ages = HashMap::new();
    let mut last_spoken = None;

    for (i, &num) in (1..).zip(start_nums.iter()) {
        if let Some(last_spoken) = last_spoken {
            ages.insert(last_spoken, i);
        }
        last_spoken = Some(num);
    }

    (start_nums.len() as u32 + 1..=num_turns).fold(last_spoken.unwrap(), |last_spoken, turn| {
        ages.insert(last_spoken, turn)
            .map_or(0, |last_seen| turn - last_seen)
    }) as usize
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_examples() {
        assert_eq!(main(1, "1,3,2"), "1");
        assert_eq!(main(1, "2,1,3"), "10");
        assert_eq!(main(1, "1,2,3"), "27");
        assert_eq!(main(1, "2,3,1"), "78");
        assert_eq!(main(1, "3,2,1"), "438");
        assert_eq!(main(1, "3,1,2"), "1836");
    }

    #[test]
    #[cfg_attr(debug_assertions, ignore = "runs very slow on debug")]
    fn test_answers() {
        let input = read_to_string("inputs/day15.txt").unwrap();
        assert_eq!(main(1, &input), "412");
        assert_eq!(main(2, &input), "243");
    }
}
