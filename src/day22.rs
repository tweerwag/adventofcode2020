use std::collections::VecDeque;

type Input = (Vec<u32>, Vec<u32>);

pub fn main(part: usize, input: &str) -> String {
    let parsed = parse_input(input);
    match part {
        1 => part1(&parsed),
        2 => part2(&parsed),
        _ => panic!("Unknown part number"),
    }
}

fn parse_input(input: &str) -> Input {
    let mut lines = input.lines();

    // Skip the "Player 1:" line
    lines.next().unwrap();

    let mut player1 = vec![];
    while let Some(line) = lines.next() {
        if line.is_empty() {
            break;
        }
        player1.push(line.parse().unwrap());
    }

    // Skip the "Player 2:" line
    lines.next().unwrap();

    let mut player2 = vec![];
    while let Some(line) = lines.next() {
        if line.is_empty() {
            break;
        }
        player2.push(line.parse().unwrap());
    }
    (player1, player2)
}

fn part1((player1, player2): &Input) -> String {
    let mut deck1: VecDeque<_> = player1.iter().copied().collect();
    let mut deck2: VecDeque<_> = player2.iter().copied().collect();

    while !deck1.is_empty() && !deck2.is_empty() {
        // Draw cards
        let card1 = deck1.pop_front().unwrap();
        let card2 = deck2.pop_front().unwrap();

        if card1 > card2 {
            deck1.push_back(card1);
            deck1.push_back(card2);
        } else {
            deck2.push_back(card2);
            deck2.push_back(card1);
        }
    }

    let winning_deck = if deck1.is_empty() { &deck2 } else { &deck1 };
    let score = winning_deck
        .iter()
        .rev()
        .enumerate()
        .map(|(i, &card)| (i + 1) * card as usize)
        .sum::<usize>();
    format!("{}", score)
}

fn part2((player1, player2): &Input) -> String {
    let mut deck1: VecDeque<_> = player1.iter().copied().collect();
    let mut deck2: VecDeque<_> = player2.iter().copied().collect();

    fn play_game(deck1: &mut VecDeque<u32>, deck2: &mut VecDeque<u32>) -> bool {
        let mut prev_states = vec![];

        while !deck1.is_empty() && !deck2.is_empty() {
            if prev_states
                .iter()
                .any(|(prev_deck1, prev_deck2)| deck1 == prev_deck1 && deck2 == prev_deck2)
            {
                // println!("Previous state encountered: player 1 wins");
                return true;
            }
            prev_states.push((deck1.clone(), deck2.clone()));
            // println!("deck1: [{}] deck2: [{}]", deck1.iter().map(|x| format!("{}", x)).collect::<Vec<_>>().join(", "), deck2.iter().map(|x| format!("{}", x)).collect::<Vec<_>>().join(", "));

            // Draw cards
            let card1 = deck1.pop_front().unwrap();
            let card2 = deck2.pop_front().unwrap();

            let player1_won;
            if deck1.len() >= card1 as usize && deck2.len() >= card2 as usize {
                // Recurse
                let mut new_deck1 = deck1.iter().copied().take(card1 as usize).collect();
                let mut new_deck2 = deck2.iter().copied().take(card2 as usize).collect();
                // println!("Starting recursive combat: cards {} and {}", card1, card2);
                player1_won = play_game(&mut new_deck1, &mut new_deck2);
                // println!("Recursive combat: player 1 won: {}", player1_won);
            } else {
                // Old combat rules
                player1_won = card1 > card2;
                // println!("Old combat: cards {} and {} (player 1 won: {})", card1, card2, player1_won);
            }
            if player1_won {
                deck1.push_back(card1);
                deck1.push_back(card2);
            } else {
                deck2.push_back(card2);
                deck2.push_back(card1);
            }
        }
        return deck2.is_empty();
    }

    let player1_won = play_game(&mut deck1, &mut deck2);
    let winning_deck = if player1_won { &deck1 } else { &deck2 };
    let score = winning_deck
        .iter()
        .rev()
        .enumerate()
        .map(|(i, &card)| (i + 1) * card as usize)
        .sum::<usize>();
    format!("{}", score)
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_examples() {
        let input = "Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10";
        assert_eq!(main(1, input), "306");
        assert_eq!(main(2, input), "291");
    }

    #[test]
    #[cfg_attr(debug_assertions, ignore = "runs very slow on debug")]
    fn test_answers() {
        let input = read_to_string("inputs/day22.txt").unwrap();
        assert_eq!(main(1, &input), "33772");
        assert_eq!(main(2, &input), "35070");
    }
}
