pub trait IteratorFoldFirst: Iterator
where
    Self: Sized,
{
    fn my_fold_first<F>(mut self, f: F) -> Option<Self::Item>
    where
        F: FnMut(Self::Item, Self::Item) -> Self::Item,
    {
        let acc = self.next()?;
        Some(self.fold(acc, f))
    }
}

impl<T: Iterator> IteratorFoldFirst for T {}

pub fn one_bits_to_idxs(mut x: u64) -> Vec<usize> {
    let mut res = vec![];
    for i in 0.. {
        if x & 1 == 1 {
            res.push(i);
        }
        x >>= 1;
        if x == 0 {
            break;
        }
    }
    res
}
