use std::collections::HashMap;

use regex::Regex;

pub fn main(part: usize, input: &str) -> String {
    let (rules, strs) = parse_input(input);
    format!(
        "{}",
        match part {
            1 => do_problem(false, &rules, &strs),
            2 => do_problem(true, &rules, &strs),
            _ => panic!("Unknown part number"),
        }
    )
}

#[derive(Debug, Copy, Clone, PartialEq)]
enum Token {
    Terminal(char),
    Nonterminal1(u32),
    Nonterminal2(u32, u32),
}

type Rules = HashMap<u32, (Token, Option<Token>)>;

fn parse_input(input: &str) -> (Rules, Vec<String>) {
    let re = Regex::new(
        r#"(?x)
        (?P<rule_num>\d+):\s+
        (?:
            "(?P<term>.)" |
            (?P<nt1>\d+) (?:\s+(?P<nt2>\d+))?
                (?:\s+ \| \s+ (?P<nt3>\d+) (?:\s+(?P<nt4>\d+))?)?
        )
    "#,
    )
    .unwrap();

    let mut lines = input.lines();
    let mut rules = HashMap::new();

    while let Some(line) = lines.next() {
        if line.is_empty() {
            break;
        }

        let caps = re.captures(line).unwrap();
        let rule_num = caps["rule_num"].parse().unwrap();
        let toks = if let Some(t) = caps.name("term") {
            (Token::Terminal(t.as_str().chars().next().unwrap()), None)
        } else {
            let nt1 = caps["nt1"].parse().unwrap();
            let tok1 = if let Some(nt2) = caps.name("nt2") {
                Token::Nonterminal2(nt1, nt2.as_str().parse().unwrap())
            } else {
                Token::Nonterminal1(nt1)
            };
            let tok2 = if let Some(nt3) = caps.name("nt3") {
                let nt3 = nt3.as_str().parse().unwrap();
                Some(if let Some(nt4) = caps.name("nt4") {
                    Token::Nonterminal2(nt3, nt4.as_str().parse().unwrap())
                } else {
                    Token::Nonterminal1(nt3)
                })
            } else {
                None
            };
            (tok1, tok2)
        };
        rules.insert(rule_num, toks);
    }
    let strs = lines.map(str::to_owned).collect();
    (rules, strs)
}

fn do_problem(is_part2: bool, rules: &Rules, strs: &[String]) -> usize {
    fn rec<'a>(is_part2: bool, rules: &'a Rules, input: &'a str, cur_rule: u32) -> Vec<&'a str> {
        // println!("Matching rule {} on \"{}\"", cur_rule, input);

        if is_part2 {
            if cur_rule == 8 {
                // Special recursive rule 8: 42 | 42 8
                let mut res = rec(is_part2, rules, input, 42);
                let mut rem_inps = res.clone();
                while !rem_inps.is_empty() {
                    rem_inps = rem_inps
                        .into_iter()
                        .flat_map(|s| rec(is_part2, rules, s, 42))
                        .collect();
                    res.extend_from_slice(&rem_inps);
                }
                return res;
            }
            if cur_rule == 11 {
                // Special recursive rule 11: 42 31 | 42 11 31
                fn match11<'a>(rules: &'a Rules, input: &'a str) -> Vec<&'a str> {
                    // First match 42
                    let mut first = rec(true, rules, input, 42);
                    // Then try another recursive 11
                    let first_ext = first
                        .iter()
                        .flat_map(|s| match11(rules, s))
                        .collect::<Vec<_>>();
                    // Add the recursive results to the single 42
                    first.extend_from_slice(&first_ext);
                    // Match 31 on all
                    first
                        .into_iter()
                        .flat_map(|s| rec(true, rules, s, 31))
                        .collect()
                }
                return match11(rules, input);
            }
        }

        let (tok1, tok2) = rules[&cur_rule];

        // First try matching tok1
        let mut rem = match tok1 {
            Token::Terminal(c) if input.starts_with(c) => vec![&input[1..]],
            Token::Nonterminal1(nt1) => rec(is_part2, rules, input, nt1),
            Token::Nonterminal2(nt1, nt2) => {
                let v = rec(is_part2, rules, input, nt1);
                v.iter()
                    .flat_map(|rem_inp| rec(is_part2, rules, rem_inp, nt2))
                    .collect()
            }
            _ => vec![],
        };

        // And try matching tok2
        if let Some(tok2) = tok2 {
            rem.extend(match tok2 {
                Token::Terminal(c) if input.starts_with(c) => vec![&input[1..]],
                Token::Nonterminal1(nt1) => rec(is_part2, rules, input, nt1),
                Token::Nonterminal2(nt1, nt2) => {
                    let v = rec(is_part2, rules, input, nt1);
                    v.iter()
                        .flat_map(|rem_inp| rec(is_part2, rules, rem_inp, nt2))
                        .collect()
                }
                _ => vec![],
            });
        }
        rem
    }
    strs.iter()
        .filter(|&s| rec(is_part2, rules, s, 0).contains(&""))
        .count()
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_answers() {
        let input = read_to_string("inputs/day19.txt").unwrap();
        assert_eq!(main(1, &input), "151");
        assert_eq!(main(2, &input), "386");
    }
}
