use std::collections::HashMap;

struct CompressedRoom {
    seats: HashMap<(isize, isize), bool>,
    adjacencies: HashMap<(isize, isize), Vec<(isize, isize)>>,
}

impl CompressedRoom {
    fn parse(input: &str, rays: bool) -> CompressedRoom {
        let mut seats = HashMap::new();
        for (y, line) in input.lines().enumerate() {
            for (x, c) in line.chars().enumerate() {
                match c {
                    '.' => {}
                    'L' => {
                        seats.insert((x as isize, y as isize), false);
                    }
                    '#' => {
                        seats.insert((x as isize, y as isize), true);
                    }
                    _ => panic!("Unknown character '{}'", c),
                }
            }
        }

        let mut adjacencies = HashMap::new();
        if !seats.is_empty() {
            let max_x = seats.keys().map(|&(x, _)| x).max().unwrap();
            let max_y = seats.keys().map(|&(_, y)| y).max().unwrap();
            dbg!(max_x, max_y);

            for &(x, y) in seats.keys() {
                const DIRECTIONS: &[(isize, isize)] = &[
                    (1, 0),
                    (1, 1),
                    (0, 1),
                    (-1, 1),
                    (-1, 0),
                    (-1, -1),
                    (0, -1),
                    (1, -1),
                ];
                let mut v = vec![];
                for &(dir_x, dir_y) in DIRECTIONS {
                    if rays {
                        let (mut new_x, mut new_y) = (x + dir_x, y + dir_y);
                        'a: while new_x >= 0 && new_x <= max_x && new_y >= 0 && new_y <= max_y {
                            if seats.contains_key(&(new_x, new_y)) {
                                v.push((new_x, new_y));
                                break 'a;
                            }
                            new_x += dir_x;
                            new_y += dir_y;
                        }
                    } else {
                        let q = (x + dir_x, y + dir_y);
                        if seats.contains_key(&q) {
                            v.push(q);
                        }
                    }
                }
                adjacencies.insert((x, y), v);
            }
        }

        CompressedRoom { seats, adjacencies }
    }

    fn step(&mut self, occupied_threshold: usize) -> bool {
        let mut new_seats = HashMap::with_capacity(self.seats.len());
        let mut changed = false;
        for (&p, &occ) in &self.seats {
            let occupied_adj = self.adjacencies[&p]
                .iter()
                .filter(|&q| self.seats[q])
                .count();
            if !occ && occupied_adj == 0 {
                new_seats.insert(p, true);
                changed = true;
            } else if occ && occupied_adj >= occupied_threshold {
                new_seats.insert(p, false);
                changed = true;
            } else {
                new_seats.insert(p, occ);
            }
        }
        self.seats = new_seats;
        changed
    }
}

pub fn main(part: usize, input: &str) -> String {
    let (rays, occupied_threshold) = match part {
        1 => (false, 4),
        2 => (true, 5),
        _ => panic!("Unknown part number"),
    };

    let mut room = CompressedRoom::parse(input, rays);
    while room.step(occupied_threshold) {}

    format!("{}", room.seats.values().filter(|&&occ| occ).count())
}

#[cfg(test)]
mod test {
    use std::fs::read_to_string;

    use super::*;

    #[test]
    fn test_examples() {
        let input = "L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL";
        assert_eq!(main(1, &input), "37");
        assert_eq!(main(2, &input), "26");
    }

    #[test]
    #[cfg_attr(debug_assertions, ignore = "runs very slow on debug")]
    fn test_answers() {
        let input = read_to_string("inputs/day11.txt").unwrap();
        assert_eq!(main(1, &input), "2261");
        assert_eq!(main(2, &input), "2039");
    }
}
